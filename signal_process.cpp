#include "stdafx.h"
#include "signal_process.h"

#define M_PI 3.14159265359

/// Provede autokorelaci signalu.
void autocorrelation(float* buffer, int buffer_len, float* result_buffer)
{
  memset(result_buffer, 0, buffer_len * 2 * sizeof(float));
  
  for (int i = 0; i < buffer_len; i++)
  {
    float value = buffer[buffer_len - 1 - i];
    for (int j = 0; j < buffer_len; j++)
    {
      result_buffer[j + i] += buffer[j] * value;
    }
  }
}

/// Nalezne opakujici se spicky v signalu.
float find_freq(float* autocor_buffer, int buffer_len, float maxfreq, int samplefreq)
{
  float threshold;
  int state = 0;
  float old_val, cur_val = 0.f;
  int counter;
  int mincounter = (int)(samplefreq / maxfreq);

  counter = 0;
  for (int i = buffer_len - 1; i >= 0; i--)
  {
    old_val = cur_val;
    cur_val = autocor_buffer[i];    

    // Peak Detect State Machine
    if (state == 2 && cur_val <= old_val) 
    {
      return (float)samplefreq / (counter - 1);
    }
    else if (state == 1 && counter > mincounter && cur_val > threshold && cur_val > old_val) 
    {
      state = 2;
    }
    else if (i == buffer_len - 1) 
    {
      threshold = cur_val * 0.5f;
      state = 1;
    }

    counter++;
  }
  return 0.f;
}


// tabulka frekvenci tonu
const float tone_freq[] = 
{
  65.41f, 69.30f, 73.42f, 77.78f, 82.41f, 87.31f, 92.50f, 98.00f, 103.8f, 110.0f, 116.5f, 123.5f,
  130.8f, 138.6f, 146.8f, 155.6f, 164.8f, 174.6f, 185.0f, 196.0f, 207.7f, 220.0f, 233.1f, 246.9f,
  261.6f, 277.2f, 293.7f, 311.1f, 329.6f, 349.2f, 370.0f, 392.0f, 415.3f, 440.0f, 466.2f, 493.9f,
  523.3f, 554.4f, 587.3f, 622.3f, 659.3f, 698.5f, 740.0f, 784.0f, 830.6f, 880.0f, 932.3f, 987.8f
};

// nazvy not
const char* tone_name[] =
{
  "C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B"
};

// zjisteni odchylky v logaritmickem merintu (cents)
const float get_cents(float a, float b)
{
  return (1200.f / log(2.f)) * log(b / a);
}

// nazev noty a odchylka podle zadane frekvence
const char* get_tone_and_err(float freq)
{
  static char str[16];
  const char* name = "\0";

  int lo = 0, hi = 12 * 4 - 1;
  int mid;
  while (lo < hi)
  {
    mid = (lo + hi) >> 1;
    if (freq < tone_freq[mid])
    {
      hi = mid - 1;
    }
    else
    {
      lo = mid + 1;
    }
  }

  float cents; 
  do
  {
    cents = get_cents(tone_freq[mid], freq);

    if (mid == 12 * 4 - 1)
      break;
    else if (cents > 51.f)
      mid++;
    else if (mid == 0)
      break;
    else if (cents < -51.f)
      mid--;
    else break;

  } while(1);
  name = tone_name[mid % 12];

  sprintf(str, "%2s %+2.f c", name, cents);
  return str;
}

/// Priprava bufferu pro FFT. Buffer dest obsahuje realne i imaginarni koeficienty => pouziva 2x vice mista.
void prepare_fft_buffer(float* source, int source_len, float* dest)
{
  for (int i = 0, j = 0;
    i < source_len;
    i++, j += 2)
  {
    dest[j] = source[i];
    dest[j+1] = 0.f;
  }
}

/// Zameni hodnoty a, b.
inline void swap(float& a, float& b)
{
  float temp = a;
  a = b;
  b = temp;
}

/// Vypocet FFT (in place).
void compute_fft_in_place(float* data, unsigned long nn)
{
  unsigned long n, mmax, m, j, istep, i;
  float wtemp, wr, wpr, wpi, wi, theta;
  float tempr, tempi;

  // reverse-binary reindexing
  n = nn<<1;
  j=1;
  for (i=1; i<n; i+=2) 
  {
    if (j>i) 
    {
      swap(data[j-1], data[i-1]);
      swap(data[j], data[i]);
    }
    m = nn;
    while (m>=2 && j>m) 
    {
      j -= m;
      m >>= 1;
    }
    j += m;
  };

  // here begins the Danielson-Lanczos section
  mmax=2;
  while (n>mmax) 
  {
    istep = mmax<<1;
    theta = -(2.f*(float)M_PI/mmax);
    wtemp = sin(0.5f*theta);
    wpr = -2.f*wtemp*wtemp;
    wpi = sin(theta);
    wr = 1.0;
    wi = 0.0;
    for (m=1; m < mmax; m += 2) 
    {
      for (i=m; i <= n; i += istep) 
      {
        j=i+mmax;
        tempr = wr*data[j-1] - wi*data[j];
        tempi = wr * data[j] + wi*data[j-1];

        data[j-1] = data[i-1] - tempr;
        data[j] = data[i] - tempi;
        data[i-1] += tempr;
        data[i] += tempi;
      }
      wtemp=wr;
      wr += wr*wpr - wi*wpi;
      wi += wi*wpr + wtemp*wpi;
    }
    mmax=istep;
  }
}

/// Nalezeni frekvence ve vysledku FFT.
float find_freq_fft(float* data, int nn, float minfreq, float maxfreq, int samplefreq)
{
  int n = nn << 1;
  int maxn = min((int)(n * maxfreq / samplefreq) + 1, n / 2);
  int minn = (int)(n * minfreq / samplefreq);

  float maxval = 0.f;
  int maxi = 0;

  float localsum = 0.f;
  const int localsum_range = 20*2;
  for (int i = minn; i < maxn; i += 2)
  {
    if (i-localsum_range > 0)
      localsum -= (data[i-localsum_range] * data[i-localsum_range] + data[i+1-localsum_range] * data[i+1-localsum_range]) * (1.f - (float)(i - localsum_range - minn) / (maxn - minn));
    float val = (data[i] * data[i] + data[i+1] * data[i+1]) * (1.f - 0.5f * (float)(i - localsum_range - minn) / (maxn - minn));
    localsum += val;
    if (localsum > maxval)
    {
      maxi = i;
      maxval = localsum;
    }
  }

  /*
  float val = sqrt(data[maxi] * data[maxi] + data[maxi+1] * data[maxi+1]);
  float valminus1 = sqrt(data[maxi-2] * data[maxi-2] + data[maxi-1] * data[maxi-1]);
  float valplus1 = sqrt(data[maxi+2] * data[maxi+2] + data[maxi+3] * data[maxi+3]);
  */
  maxi = (maxi - localsum_range/2) >> 1;
  return (float)samplefreq * (0.5f + maxi) / nn;
}

/// Aplikuje Bartlettovo okno na signal.
void bartlett_window(float* data, int n)
{
  int n2 = n / 2;
  float inv_n2 = 1.f / n2;
  for (int i = 0; i < n2; i++)
  {
    data[i] = data[i] * i * inv_n2;
  }

  for (int i = n2; i < n; i++)
  {
    data[i] = data[i] * (1.f - (i - n2) * inv_n2);
  }
}