#pragma once

/// Provede autokorelaci signalu.
void autocorrelation(float* buffer, int buffer_len, float* result_buffer);
/// Nalezne opakujici se spicky v signalu.
float find_freq(float* autocor_buffer, int buffer_len, float maxfreq, int samplefreq);
/// Ziskani jmena tonu.
const char* get_tone_and_err(float freq);
/// Priprava bufferu pro FFT. Buffer dest obsahuje realne i imaginarni koeficienty => pouziva 2x vice mista.
void prepare_fft_buffer(float* source, int source_len, float* dest);
/// Vypocet FFT (in place).
void compute_fft_in_place(float* data, unsigned long nn);
/// Nalezeni frekvence ve vysledku FFT.
float find_freq_fft(float* data, int nn, float minfreq, float maxfreq, int samplefreq);
/// Aplikuje Bartlettovo okno na signal.
void bartlett_window(float* data, int n);