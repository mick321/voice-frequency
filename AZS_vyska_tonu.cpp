#include "stdafx.h"
#include "signal_process.h"

const int    buffercount = 16;
HWAVEIN      hWaveIn = NULL;
WAVEHDR      WaveInHdr[buffercount];
MMRESULT     mmresult;

// omezeni: maximalni hledana frekvence zakladniho tonu hlasu
#define VOICE_MIN_FREQ 60
#define VOICE_MAX_FREQ 1000
#define MAX_HISTORY 11

const int    sampleRate = 44100;   // pocet vzorku za vterinu
const int    bufferLength = 2048 * (44100 / sampleRate);  // coz odpovida ~92ms pri 44100Hz
const int    fftSamples = 1024 * 32 * 2;

short int*   buffer[buffercount]; // jednotlive buffery
float*       fbuffer[buffercount];
int          active_buffer = 0;   // index aktivniho bufferu
int          queueBufferIndex = 0;
float        tempbuffer[fftSamples]; // docasny buffer

float        frequencyHistoryValues[MAX_HISTORY]; // historie signalu
int          frequencyHistoryCount = 0;
bool         finished = false;

enum TMethod
{
  method_unknown,
  method_autocorrelation,
  method_fft
};

TMethod used_method = method_unknown;

#define SAFE_DELETE(p) if (p) { delete (p); (p) = NULL; }

///
/// Vytisteni chyby (ve spravnem formatu).
///
void print_error(const char* errstr)
{
  printf("[error]  %s\n", errstr);
}

///
/// Posun historicky namerenych frekvenci.
///
void freq_history_shift()
{
  if (frequencyHistoryCount < MAX_HISTORY)
    return;

  for (int i = 0; i < frequencyHistoryCount - 1; i++)
  {
    frequencyHistoryValues[i] = frequencyHistoryValues[i + 1];
  }
  frequencyHistoryCount--;
}

///
/// Zarazeni namerene frekvence do historie.
///
void freq_history_push(float value)
{
  if (frequencyHistoryCount >= MAX_HISTORY)
  {
    return;
  }

  frequencyHistoryValues[frequencyHistoryCount] = value;
  frequencyHistoryCount++;
}

///
/// Ziskani medianu z historie.
///
float freq_history_compute_median()
{
  if (frequencyHistoryCount < MAX_HISTORY / 2)
    return 0.f;

  float arr_copy[MAX_HISTORY];
  float temp;
  // copy array
  memcpy(arr_copy, frequencyHistoryValues, frequencyHistoryCount * sizeof(float));
  // insert sort
  for (int i = 1; i < frequencyHistoryCount; i++)
  {
    int j = i;
    while (j > 0)
    {
      if (arr_copy[j] < arr_copy[j-1])
      {
        temp = arr_copy[j-1];
        arr_copy[j-1] = arr_copy[j];
        arr_copy[j] = temp;
      }
      else
      {
        break;
      }
    }
  }

  // return avg mid
  return 0.333333f * (arr_copy[frequencyHistoryCount >> 1] + arr_copy[(frequencyHistoryCount >> 1) + 1] + arr_copy[(frequencyHistoryCount >> 1) - 1]);
}

///
/// Zpracovani dat z bufferu.
///
void ProcessData()
{
  // 1. prevedeni na rozsah [-1,1]
  short int* p_bfr = buffer[active_buffer];
  float* p_fbfr = fbuffer[active_buffer];
  float mul_coeff = 1.f / (128 * 256);
  for (int i = 0; i < bufferLength; i++)
  {
    p_fbfr[i] = mul_coeff * p_bfr[i];
  }

  float freq;
  switch (used_method)
  {
  case method_unknown:
    freq = 0.f;
    break;
  case method_autocorrelation:
    bartlett_window(p_fbfr, bufferLength);
    autocorrelation(p_fbfr, bufferLength, tempbuffer);
    freq = find_freq(tempbuffer, bufferLength, VOICE_MAX_FREQ, sampleRate);
    break;
  case method_fft:
    bartlett_window(p_fbfr, bufferLength);
    prepare_fft_buffer(p_fbfr, bufferLength, tempbuffer);
    memset(tempbuffer + (bufferLength * 2), 0, (fftSamples - bufferLength * 2) * sizeof(float));
    compute_fft_in_place(tempbuffer, fftSamples/2);
    freq = find_freq_fft(tempbuffer, fftSamples/2, VOICE_MIN_FREQ, VOICE_MAX_FREQ, sampleRate);
    break;
  }

  freq_history_shift();
  freq_history_push(freq);
  float freq_median = freq_history_compute_median();
  if (freq_median > 0.f)
  {
    printf("\rMeasured frequency: %6.0f Hz (%s)   ", freq_median, get_tone_and_err(freq_median));
  }
  else
  {
    printf("\rMeasured frequency:     ?? Hz                        ");
  }
}

///
/// Zarazeni bufferu.
///
bool enqueue_buffer(short int* buffer, WAVEHDR& header, bool unprepare)
{
  // zruseni predchozi hlavicky
  if (unprepare)
  {
    waveInUnprepareHeader(hWaveIn, &header, sizeof(WAVEHDR));
  }

  // nastaveni hlavicky
  header.lpData = (LPSTR)buffer; // BUFFER
  header.dwBufferLength = bufferLength * sizeof(short int); // BUFFER LENGTH
  header.dwBytesRecorded = 0;
  header.dwUser = 0L;
  header.dwFlags = 0L;
  header.dwLoops = 0L;
  waveInPrepareHeader(hWaveIn, &header, sizeof(WAVEHDR));

  // samotne zarazeni bufferu
  mmresult = waveInAddBuffer(hWaveIn, &header, sizeof(WAVEHDR));
  if (mmresult)
  {
    print_error("Failed to add buffer to input device.\n");
    return false;
  }
  return true;
}

///
/// Zaradi volne buffery do fronty.
///
void tryEnqueueBuffers()
{
  if (queueBufferIndex == active_buffer)
    return;
  
  if (queueBufferIndex < active_buffer)
  {
    for (int i = queueBufferIndex; i < active_buffer; i++)
      enqueue_buffer(buffer[i], WaveInHdr[i], true);
  }
  else
  {
    for (int i = queueBufferIndex; i < buffercount; i++)
      enqueue_buffer(buffer[i], WaveInHdr[i], true);
    for (int i = 0; i < active_buffer; i++)
      enqueue_buffer(buffer[i], WaveInHdr[i], true);
  }
  queueBufferIndex = active_buffer;
}

///
/// Callback funkce.
///
void CALLBACK waveInProc(HWAVEIN hwi, UINT uMsg, DWORD_PTR dwInstance, DWORD_PTR dwParam1, DWORD_PTR dwParam2)
{
  if (uMsg == WIM_DATA)
  {
    ProcessData();
    active_buffer = (active_buffer + 1) % buffercount;
  }
}

///
/// Inicializace zaznamu.
///
bool init_device()
{
  // priprava bufferu
  for (int i = 0; i < buffercount; i++)
  {
    buffer[i] = new short int[bufferLength];
    fbuffer[i] = new float[bufferLength];
  }

  // specifikace parametru pro nahravani
  WAVEFORMATEX pFormat;
  memset(&pFormat, 0, sizeof(WAVEFORMATEX));
  pFormat.wFormatTag = WAVE_FORMAT_PCM;     
  pFormat.nChannels = 1;                    // mono
  pFormat.nSamplesPerSec = sampleRate;      // 44100
  pFormat.nAvgBytesPerSec = sampleRate*2;   // = nSamplesPerSec * n.Channels * wBitsPerSample/8
  pFormat.nBlockAlign = 2;                  // = n.Channels * wBitsPerSample/8
  pFormat.wBitsPerSample = 16;              //  16 for high quality, 8 for telephone-grade
  pFormat.cbSize = 0;

  int num = waveInGetNumDevs();
  if (num == 0)
  {
    print_error("No input device available.");
    return false;
  }

  mmresult = waveInOpen(&hWaveIn, WAVE_MAPPER, &pFormat,
                       (DWORD_PTR)&waveInProc, 0L, WAVE_FORMAT_DIRECT | CALLBACK_FUNCTION);
  if (mmresult)
  {
    char fault[256];
    waveInGetErrorTextA(mmresult, fault, 256);
    print_error(fault);
    return false;
  }

  active_buffer = 0;
  // pridani bufferu
  for (int i = 0; i < buffercount; i++)
  {
    if (!enqueue_buffer(buffer[i], WaveInHdr[i], false))
    {
      print_error("Failed to enqueue buffers.");
      return false;
    }
  }

  // Commence sampling input
  mmresult = waveInStart(hWaveIn);
  if (mmresult)
  {
    print_error("Failed to start recording.");
    return false;
  }

  switch (used_method)
  {
  case method_autocorrelation:
    printf("Used method: AUTOCORRELATION.\n");
    break;
  case method_fft:
    printf("Used method: FFT.\n");
    break;
  }

  printf("Capturing sound from input device...\n");
  return true;
}

///
/// Deinicializace zarizeni.
///
bool deinit_device()
{
  if (hWaveIn)
  {
    for (int i = 0; i < buffercount; i++)
      do {} while (waveInUnprepareHeader(hWaveIn, &WaveInHdr[i], sizeof(WAVEHDR)) == WAVERR_STILLPLAYING);
    waveInClose(hWaveIn);
    hWaveIn = NULL;
  }

  for (int i = 0; i < buffercount; i++)
  {
    SAFE_DELETE(buffer[i]);
    SAFE_DELETE(fbuffer[i]);
  }

  return true;
}

BOOL CtrlHandler( DWORD fdwCtrlType ) 
{
  finished = true;
  return true;
}

bool ProcessCommandLine(int argc, _TCHAR* argv[])
{
  used_method = method_unknown;

  for (int i = 1; i < argc; i++)
  {
    if (!strcmp(argv[i], "-fft"))
      used_method = method_fft;
    else if (!strcmp(argv[i], "-ac") || !strcmp(argv[i], "-autocorrelation"))
      used_method = method_autocorrelation;
  }

  if (used_method == method_unknown)
  {
    char c = '\0';
    do
    {
      char s[64];
      printf("Enter method (0: autocorrelation, 1: FFT): ");
      scanf("%s", s);
      c = s[0];
    } while(c != '0' && c != '1');

    if (c == '0')
      used_method = method_autocorrelation;
    else
      used_method = method_fft;
  }
  return true;
}

///
/// Vstupni funkce programu.
///
int _tmain(int argc, _TCHAR* argv[])
{
  if (!ProcessCommandLine(argc, argv))
  {
    print_error("Wrong command line arguments.");
    return -1;
  }

  memset(buffer, 0, sizeof(short int *) * buffercount);
  bool init_ok = true;
  if (!init_device())
  {
    print_error("Cannot access default input device.");
    init_ok = false;
  }

  if (init_ok)
  {
    SetConsoleCtrlHandler((PHANDLER_ROUTINE)&CtrlHandler, true);
    printf("\n");
    while (!finished)
    {
      tryEnqueueBuffers();
      Sleep(1);
    }
  }

  if (!deinit_device())
  {
    print_error("Problem during deinitialization.");
  }

  printf("\n");
	return 0;
}

